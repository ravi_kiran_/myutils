import json
import pickle
import time
from functools import wraps
from itertools import chain, islice


def read_json(file_path, mode='r'):
    """
    Opens a json file and returns the content as a dictionary.
    """
    with open(file_path, mode) as o:
        return json.load(o)


def read_pickle(file_path, mode='rb'):
    with open(file_path, mode) as o:
        return pickle.load(o)


def params_to_dict(**kwargs):
    """
    Converts parameters to dictionary.
    eg: asd=123.123, wer='fdsf'  --->  {'asd': 123.123, 'wer': 'fdsf'}
    """
    print(kwargs)
    return kwargs


def dict_to_params(dict_):
    """
    Converts dictionary to function parameters.
    eg: {'asd': 123.123, 'wer': 'fdsf'}  --->  asd=123.123, wer='fdsf'
    """
    params = []
    for k, v in dict_.items():
        if not isinstance(k, str):
            k = str(k)
        val_num = "{k}={v}" if (isinstance(v, int) or isinstance(v, float)) else "{k}='{v}'"
        params.append(val_num.format(k=k, v=v))
    result = ', '.join(params)
    print(result)
    return result


def get_batches(iterable, batch_size=100):
    """
    Function return generator of generators convert iterable into chunks of batch_size.

    Usage:
    >> batches = get_batches(iterable, 10)
    >> for batch in batches:
           for element in batch:
               print(element)

    https://stackoverflow.com/q/24527006/8603069
    """
    iterator = iter(iterable)
    for first_element in iterator:
        yield chain([first_element], islice(iterator, batch_size - 1))


# create a function that will take a function, it's parameters and executes it in batches
def execute_batches():
    pass


def time_taken(original_function):
    """
    Decorator for calculating the time taken by a decorated function.
    """
    @wraps(original_function)
    def wrapper_function(*args, **kwargs):
        start_time = time.time()
        result = original_function(*args, **kwargs)
        end_time = time.time()
        print('Time taken by {} is {} sec'.format(original_function.__name__, end_time - start_time))
        return result
    return wrapper_function
