from distutils.core import setup

setup(
    name='myutils',
    version='1.0',
    description='utilities package',
    author='Ravi Kiran',
    author_email='ravikiran.audi@gmail.com',
    packages=['myutils']
)